# Add requests to job

curl --location --request POST 'https://content-api.staging.lionbridge.com/v2/jobs/H5GrB7C9jk/requests/add' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '{
    "requestName": "test request",
    "sourceNativeId": "en",
    "sourceNativeLanguageCode": "en-US",
    "targetNativeLanguageCodes": [
        "fr-FR"
    ],
    "targetNativeIds": [
        "fr"
    ],
      "fmsFileId": "%%%FMS FILE ID%%%"
}'

#Response

{
    "_embedded": {
        "requests": [
            {
                "requestId": "9elt-x8WTZWETdpPUtwA2g",
                "jobId": "H5GrB7C9jk",
                "requestName": "test request",
                "statusCode": "CREATED",
                "hasError": false,
                "sourceNativeId": "en",
                "sourceNativeLanguageCode": "en-US",
                "targetNativeId": "fr",
                "targetNativeLanguageCode": "fr-FR",
                "createdDate": "2021-03-02T07:07:42.096Z",
                "modifiedDate": "2021-03-02T07:07:42.096Z",
                "fileId": "%%%FMS FILE ID%%%",
                "fileType": "xml"
            }
        ]
    }
}
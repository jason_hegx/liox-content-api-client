#Complete a job

curl --location --request PUT 'https://content-api.staging.lionbridge.com/v2/jobs/WfF4sGC9jd/complete' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \
--data-raw '"<object>"'

#Response

{
    "jobId": "WfF4sGC9jd",
    "jobName": "Test job SK-SP QA-9th Feb2021",
    "description": "New test Job for testing",
    "statusCode": "COMPLETED",
    "hasError": false,
    "latestErrorMessage": "",
    "submitterId": "sp_test_user_002",
    "creatorId": "sp_test_user_002",
    "providerId": "%%%PROVIDER_ID_STRING%%%",
    "poReference": "9692",
    "dueDate": "2020-01-01T00:00:00Z",
    "createdDate": "2021-03-02T07:29:07.377Z",
    "modifiedDate": "2021-03-02T08:20:37.087Z",
    "submittedDate": "2021-03-02T07:33:15.67Z",
    "archived": false,
    "shouldQuote": false,
    "providerReference": "BV-000377758",
    "siteId": "%%%SITE_ID%%%",
    "globalTrackingId": "%%%GLOBAL_TRACKING_ID%%%"
}
#Get all configured translation providers

curl --location --request GET 'https://content-api.staging.lionbridge.com/v2/providers' \
--header 'Authorization: Bearer %%%USER_TOKEN_STRING%%%' \

#Response

{
    "_embedded": {
        "providers": [
            {
                "providerId": "%%%PROVIDER_ID_STRING%%%",
                "providerName": "%%%PROVIDER_NAME_STRING%%%"
            },
            {
                "providerId": "%%%PROVIDER_ID_STRING%%%",
                "providerName": "%%%PROVIDER_NAME_STRING%%%"
            },
            {
                "providerId": "%%%PROVIDER_ID_STRING%%%",
                "providerName": "%%%PROVIDER_NAME_STRING%%%"
            },
            {
                "providerId": "%%%PROVIDER_ID_STRING%%%",
                "providerName": "%%%PROVIDER_NAME_STRING%%%"
            }
        ]
    }
}


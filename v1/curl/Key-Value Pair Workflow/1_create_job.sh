# create a new job
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' -d '{
  "jobName": "kche new job 121920170916",
  "description": "ken che new rest api job",
  "poReference": "123456",
  "providerId": "%%%PROVIDER_ID_STRING%%%",
}' 'https://content-api.lionbridge.com/v1/jobs'


# {
  # "jobId": "e093fbc0-ddbc-4eef-9469-aad71f23ab38",
  # "jobName": "kche new job 121920170916",
  # "description": "ken che new rest api job",
  # "statusCode": {
    # "statusCode": "CREATED"
  # },
  # "hasError": false,
  # "creatorId": "675492743",
  # "poReference": "123456",
  # "createdDate": "2017-12-19T14:18:10.521Z",
  # "modifiedDate": "2017-12-19T14:18:10.521Z",
  # "archived": false
# }
# get user token

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
  "username": "user@organization.com",
  "password": "123456"
}' 'https://content-api.lionbridge.com/v1/oauth2/token'

# {
  # "accessToken": "%%%USER_TOKEN_STRING%%%",
  # "tokenType": "bearer",
  # "expiresIn": -1
# }
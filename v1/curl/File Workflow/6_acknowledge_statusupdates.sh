curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/9fd8b4c2-9fa5-4022-ad85-d1471ac3ed45/acknowledge'

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/fa6e2865-321d-40f4-a89c-bf84f7050da8/acknowledge'

# {"updateId":"9fd8b4c2-9fa5-4022-ad85-d1471ac3ed45","jobId":"3a197821-7675-4444-89d0-e67bc274df69","requestIds":["29c9688b-1c63-40df-9214-84d6eb32e5b9"],"acknowledged":true,"statusCode":{"statusCode":"SENDING"},"updateTime":"2017-12-20T20:38:37.010Z","hasError":false}
# {"updateId":"fa6e2865-321d-40f4-a89c-bf84f7050da8","jobId":"3a197821-7675-4444-89d0-e67bc274df69","requestIds":["29c9688b-1c63-40df-9214-84d6eb32e5b9"],"acknowledged":true,"statusCode":{"statusCode":"SENT_TO_PLATFORM"},"updateTime":"2017-12-20T20:38:37.989Z","hasError":false}

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/706c88ef-97c5-4465-a9f0-393d0d63cbeb/acknowledge'

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/eb0f0627-96e6-4702-bd5f-c420d92a3db9/acknowledge'

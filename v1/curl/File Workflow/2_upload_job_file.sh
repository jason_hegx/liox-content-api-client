curl -X POST --header 'Content-Type: multipart/form-data' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' -F 'sourceFile=@sample.txt' 'https://content-api.lionbridge.com/v1/jobs/3a197821-7675-4444-89d0-e67bc274df69/upload?fileName=sample.txt&fileType=text%2Fplain' -o output.json

# {
  # "fileId": "fa772816-2566-4b2a-83c6-19d8d073b31f",
  # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
  # "filename": "sample.txt",
  # "filetype": "text/plain"
# }
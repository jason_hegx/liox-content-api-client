curl -X GET --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates' -o output.json

# [
  # {
    # "updateId": "9fd8b4c2-9fa5-4022-ad85-d1471ac3ed45",
    # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
    # "requestIds": [
      # "29c9688b-1c63-40df-9214-84d6eb32e5b9"
    # ],
    # "acknowledged": false,
    # "statusCode": {
      # "statusCode": "SENDING"
    # },
    # "updateTime": "2017-12-20T20:38:37.010Z",
    # "hasError": false
  # },
  # {
    # "updateId": "fa6e2865-321d-40f4-a89c-bf84f7050da8",
    # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
    # "requestIds": [
      # "29c9688b-1c63-40df-9214-84d6eb32e5b9"
    # ],
    # "acknowledged": false,
    # "statusCode": {
      # "statusCode": "SENT_TO_PLATFORM"
    # },
    # "updateTime": "2017-12-20T20:38:37.989Z",
    # "hasError": false
  # }
# ]

# [
  # {
    # "updateId": "706c88ef-97c5-4465-a9f0-393d0d63cbeb",
    # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
    # "requestIds": [
      # "29c9688b-1c63-40df-9214-84d6eb32e5b9"
    # ],
    # "acknowledged": false,
    # "statusCode": {
      # "statusCode": "SENT_TO_TRANSLATOR"
    # },
    # "updateTime": "2017-12-20T20:39:34.653Z",
    # "hasError": false
  # },
  # {
    # "updateId": "eb0f0627-96e6-4702-bd5f-c420d92a3db9",
    # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
    # "requestIds": [
      # "29c9688b-1c63-40df-9214-84d6eb32e5b9"
    # ],
    # "acknowledged": false,
    # "statusCode": {
      # "statusCode": "REVIEW_TRANSLATION"
    # },
    # "updateTime": "2017-12-20T20:41:36.175Z",
    # "hasError": false
  # }
# ]



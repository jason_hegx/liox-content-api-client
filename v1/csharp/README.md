### How to generate client code with Swagger ###

The Content API is specified in Swagger.  The Swagger tools available online can be used to generate client libraries for C# and other programming languages.

The API specification is at http://developers.lionbridge.com/content/docs/swagger-ui/api/v1/swagger.json

#### Steps, using C# as an example 

 1. Download the swagger.json file.
 2. On a web browser, go to https://editor.swagger.io/
 3. In the Swagger Editor, go to File->Import File, and select the downloaded json file.
 4. Once the json is imported, go to Generate Client, and select "csharp".
 5. Download and extract the client package.

#### Client package content

 - README.md:  outlines build requirements, installation, and sample usage
 - src: project of the generated C# classes
 - docs: markdown documentation generated from Swagger specification


For more information on Swagger Codegen, including how to obtain and use the command-line version of the tool, visit https://github.com/swagger-api/swagger-codegen



